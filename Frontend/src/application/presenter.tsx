import "./routes"
import *  as React from "react";
import { Router, Route, RouteComponentProps } from "react-router";
import { createBrowserHistory, History } from "history";

import { 
    PageFramePresenter
} from "./pages/general/page-frame/presenter";
import { getRegularPageConfigurations, PageConfiguration } from "./pages/general/pageConfiguration";

export class ApplicationPresenter extends React.Component {
    public render() {
        return (
            <Router history={createBrowserHistory()}>
                {getRegularPageConfigurations().map(this.renderPageRoute)}
            </Router>
        );
    }

    private renderPageRoute = (configuration: PageConfiguration) => {
        return (
            <Route 
                exact path={configuration.route} 
                component={(props: RouteComponentProps) => this.renderPage(configuration, props.history)}
                key={configuration.route}/>
            );
    };

    private renderPage = (configuration: PageConfiguration, history: History) => {
        return (
            <PageFramePresenter
                history={history}
                pageConfiguration={configuration}/>
        );
    };
}
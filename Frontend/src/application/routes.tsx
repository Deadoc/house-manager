import * as React from "react";
import { registerPageConfiguration } from "./pages/general/pageConfiguration";
import { AuthenticationPagePresenter } from "./pages/authentication/presenter";
import { HomePagePresenter } from "./pages/home/presenter";
import { DevicesPagePresenter } from "./pages/devices/presenter";

import { 
    House,
    DeviceHub,
    Lock
 } from "@material-ui/icons";

registerPageConfiguration({
    route: "/authentication",
    Icon: <Lock/>,
    title: "Authentication",
    navigable: false,
    contentProvider: history => <AuthenticationPagePresenter/>
});

registerPageConfiguration({
    route: "/",
    Icon: <House/>,
    title: "Home",
    navigable: true,
    contentProvider: history => <HomePagePresenter/>
});

registerPageConfiguration({
    route: "/devices",
    Icon: <DeviceHub/>,
    title: "Devices",
    navigable: true,
    contentProvider: history => <DevicesPagePresenter/>
});
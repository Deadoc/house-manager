import *  as React from "react";
import * as ReactDOM from "react-dom";
import { ApplicationPresenter } from "./application/presenter";
import { createStore } from "redux";
import { Provider } from "react-redux";

const store = createStore(() => {});

ReactDOM.render(
    <Provider store={store}>
        <ApplicationPresenter/>
    </Provider>,
    document.getElementById("applicationLayout")
);
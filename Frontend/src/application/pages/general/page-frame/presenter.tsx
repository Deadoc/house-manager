import *  as React from "react";
import { 
    AppBar,
    Toolbar,
    Typography,
    Drawer,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Icon
} from "@material-ui/core";
 import { getRegularPageConfigurations, PageConfiguration } from "../pageConfiguration";
 import { History } from "history";
 import { useStyles } from "../styles";

interface Props {
    history: History;
    pageConfiguration: PageConfiguration;
}

export const PageFramePresenter = (props: Props) => {
    const classes = useStyles();
    const{
        history,
        pageConfiguration
    } = props;

    return (
        <div className={classes.root}>
            {renderHeader(pageConfiguration.title, pageConfiguration.Icon)}
            {
                pageConfiguration.navigable
                    ? renderNavigationList(history, pageConfiguration)
                    : null
            }
            <main className={classes.content}>
                <div className={classes.toolbar} />
                {pageConfiguration.contentProvider(history)}
            </main>
        </div>
    );
};

const renderHeader = (title: string, icon: JSX.Element) => {
    const classes = useStyles();
    return (
        <AppBar 
            position="fixed" 
            className={classes.appBar}>
            <Toolbar>
                <Icon>{icon}</Icon>
                <Typography variant="h5" noWrap style={{marginLeft: 20}}>
                    {title}
                </Typography>
            </Toolbar>
        </AppBar>
    );
};

const renderNavigationList = (history: History, pageConfiguration: PageConfiguration) => {
    const classes = useStyles();

    return (
        <Drawer
            variant="permanent"
            className={classes.drawer}
            classes={{paper: classes.drawerPaper}}>
            <div className={classes.toolbar} />
            <List>
                {
                    getRegularPageConfigurations()
                        .filter(configuration => configuration.navigable)
                        .map(configuration => 
                            renderNavigationListItem(history, configuration, configuration == pageConfiguration))
                }
            </List>
        </Drawer>
    );
};

const renderNavigationListItem = (history: History, configuration: PageConfiguration, isSelected: boolean) => {
    const classes = useStyles();

    return (
        <ListItem button 
            key={configuration.route}
            onClick={() => history.push(configuration.route)} >
            <ListItemIcon>{configuration.Icon}</ListItemIcon>
            <ListItemText 
                primary={configuration.title} 
                className={isSelected ? classes.selectedText : undefined}/>
        </ListItem>
    );
};
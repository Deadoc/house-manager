const fs = require('fs-extra');
const path = require('path');
const environment = require('./environment.json');

const resolvePathToAbsolute = (baseDir, targetPath) => {
    if (path.isAbsolute(targetPath)) return targetPath;
    else if (baseDir) return path.join(baseDir, targetPath);
    else
        throw new Error(
            "Can't resolve relative path without base dir: " + relativePath
        );
};

const copyPublic = () => {
    console.log("Copying static files...");
    fs.copySync("public", resolvePathToAbsolute(__dirname, environment.outputDirectory));
}

const createConfiguration = () => {
    
    return {
        context: __dirname,
        devtool: "inline-source-map",
        entry: "./src/index.tsx",
        module: {
            rules: [
              {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
              }
            ]
        },
        resolve: {
            extensions: [".tsx", ".ts", ".js"]
        },
        output: {
            path: resolvePathToAbsolute(__dirname, environment.outputDirectory),
            filename: environment.outputFileName
        }
    };
}

copyPublic();

module.exports = createConfiguration();
import { makeStyles } from "@material-ui/core";

const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
    root: {
      display: 'flex',
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      background: "#4A545E"
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    content: {
      paddingLeft: theme.spacing(3)
    },
    toolbar: theme.mixins.toolbar,
    selectedText: {
        color: "#1976D2"
    }
  }));
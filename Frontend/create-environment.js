const fs = require("fs");
var program = require("commander");

program
    .option("-o, --output [directory path]", "Bundle output location directory. \"dist\" by default")
    .option("-f, --outputFile [file name]", "Bundle file name. \"application.js\" by default")
    .parse(process.argv);

const environment = {
    outputDirectory: program.output ? program.output : "dist",
    outputFileName: program.outputFile ? program.outputFile : "application.js"
};

fs.writeFile(
    "environment.json", 
    JSON.stringify(environment),
    (err) => err ? console.error(err) : "Success");
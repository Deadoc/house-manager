import { History } from "history";

const configurations: Record<string, PageConfiguration> = {};

export interface PageConfiguration {
    route: string;
    title: string;
    Icon: JSX.Element;
    navigable: boolean
    contentProvider: (history: History) => JSX.Element;
}

export const registerPageConfiguration = (configuration: PageConfiguration) => {
    if(configurations[configuration.route] != null)
        throw Error(`Route ${configuration.route} already registered!`);

    configurations[configuration.route] = configuration;
};

export const getRegularPageConfigurations = () => Object.values(configurations);